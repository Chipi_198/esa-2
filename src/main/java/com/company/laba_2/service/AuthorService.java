package com.company.laba_2.service;

import com.company.laba_2.repository.AuthorsRepository;
import com.company.laba_2.entity.Author;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class AuthorService {
    @Autowired
    private AuthorsRepository authorsRepository;

    public List<Author> authors() {
        Iterable<Author> all = authorsRepository.findAll();
        List<Author> authors = new ArrayList<>();
        all.forEach(authors::add);
        return authors;
    }

    public Boolean deleteAuthor(Integer id) {
        authorsRepository.deleteById(id);
        return true;
    }

    public Boolean updateAuthor(Author author) {
        authorsRepository.deleteById(author.getId());
        authorsRepository.save(author);
        return true;
    }

    public Boolean createAuthor(Author author) {
        authorsRepository.save(author);
        return true;
    }
}
