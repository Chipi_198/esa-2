package com.company.laba_2.сontroller;

import com.company.laba_2.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AuthorsController {
    @Autowired
    private AuthorService authorService;

    @RequestMapping(value = "/authors", method = RequestMethod.GET)
    public ModelAndView authors() {
        ModelAndView model = new ModelAndView();
        model.setViewName("authors");
        model.addObject("authors", authorService.authors());
        return model;
    }
}
